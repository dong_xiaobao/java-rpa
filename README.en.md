# java-rpa

#### Description
java版本的rpa操作。
特点：
1、纯java编写，对外库0依赖。不需要再安装其他软件。
2、开箱即用，5分钟上手，内有demo案例。
3、只有个位数的核心类。无其他冗余代码。方便后期维护。
4、支持识图、支持模板匹配（大图找小图）。算法完全自研。
5、支持开源协议，可以运用于商业系统。只要给我一个小小的关注与点赞。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
